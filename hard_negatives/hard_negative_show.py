import hickle as hkl
from const import PartInformation
from label_util import LabelUtil


def print_indices(part_name):

    hkl.load(part_name)


def load_hkl(part_name):
    return hkl.load("pipa_" + pipa_phase + "/" + part_name + "/thresholding_" + str(LabelUtil.get_thresh()) + "_indices.hkl")


def intersect(a, b):
    return list(set(a) & set(b))

if __name__ == "__main__":

    pipa_phase = "test"

    vgg_only = load_hkl(PartInformation.part_head_vgg_only)
    upper = load_hkl(PartInformation.part_upper_body)
    concat = load_hkl(PartInformation.part_concat)
    # body = load_hkl(pipa_phase, PartInformation.part_body)

    print "lengths of hn [" + PartInformation.part_head_vgg_only + "] : " + str(len(vgg_only))
    print "lengths of hn [" + PartInformation.part_upper_body + "] : " + str(len(upper))
    print "lengths of hn [" + PartInformation.part_concat + "] : " + str(len(upper))

    intersectn = intersect(vgg_only, upper)
    print "length of intersection : " + str(len(intersectn))

    intersectn = intersect(vgg_only, concat)
    print "length of intersection : " + str(len(intersectn))

    # vgg_only_examples = []
    # for item in vgg_only:
    #     if item not in intersect:
    #         vgg_only_examples.append(item)

    # upper_only_examples = []
    # for item in upper:
    #     if item not in intersect:
    #         upper_only_examples.append(item)

    # concat_examples = []
    # for item in concat:
    #     if item not in intersect:
    #         concat_examples.append(item)

    # print "vgg only : " + str(len(vgg_only_examples))
    # print "uppper only : " + str(len(upper_only_examples))
    # print len(concat_examples)

    # v_and_u =

