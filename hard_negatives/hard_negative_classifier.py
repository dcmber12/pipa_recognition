from label_util import LabelUtil
import hickle as hkl
import pickle as pkl
import numpy as np
from const import PartInformation
from sklearn.svm import SVC
from sklearn.svm import LinearSVC
from sklearn.model_selection import GridSearchCV
from data_model import Score
import shutil
import db_postproc as postproc
from sklearn.externals import joblib
from time import localtime, strftime, time


def prepare_dataset_train_test(data_type, part_name=PartInformation.part_head_vgg_only, false_per_true=0):
    """
    pipa_test indices => train data
    pipa_val indices => test data
    :param false_per_true:
    :param data_type: 0 to file names, 1 to deep feature
    :param part_name:
    :return: list of ...
    """

    # pipa_test to train data
    print "--> preparing hard negative train data"
    train_data, train_label = prepare_dataset(pipa_phase="test", part_name=part_name, false_per_true=false_per_true, feat_type=data_type)
    # train_data, train_label = prepare_dataset(pipa_phase="val", part_name=part_name, feat_type=data_type)
    print "train data [false : %d] [true : %d] \n" % (train_label.count(0), train_label.count(1))

    # pipa_val to test data
    print "--> preparing hard negative test data"
    # test_data, test_label = prepare_dataset(pipa_phase="test", part_name=part_name, feat_type=data_type)
    test_data, test_label = prepare_dataset(pipa_phase="val", part_name=part_name, false_per_true=false_per_true, feat_type=data_type)
    print "test data [false : %d] [true : %d] \n" % (test_label.count(0), test_label.count(1))
    return train_data, train_label, test_data, test_label


def prepare_dataset(pipa_phase, part_name, false_per_true, feat_type):

    thr = str(LabelUtil.get_thresh())
    total_data = []
    total_label = []
    if feat_type == 0:
        data = pkl.load(open("pipa_" + pipa_phase + "/" + part_name + "/thresholding_" + thr + "_test_file_names.p", 'r'))
        # data = [images_dir + "/" + pipa_phase + "/" + part_name + "/" + item for item in data]
    elif feat_type == 1:
        data = hkl.load("pipa_" + pipa_phase + "/" + part_name + "/thresholding_" + thr + "_test_deep_features.hkl")
    else:
        raise TypeError
    true_indices = hkl.load("pipa_" + pipa_phase + "/" + part_name + "/thresholding_" + thr + "_indices.hkl")
    data, label = refine_data(data=data, true_indices=true_indices, false_per_true=false_per_true)
    total_data += data.tolist()
    total_label += label
    assert (len(total_data) == len(total_label))
    print len(total_data)

    if feat_type == 0:
        data = pkl.load(open("pipa_" + pipa_phase + "/" + part_name + "/thresholding_" + thr + "_train_file_names.p", 'r'))
        # data = [images_dir + "/" + pipa_phase + "/" + part_name + "/" + item for item in data]
    elif feat_type == 1:
        data = hkl.load("pipa_" + pipa_phase + "/" + part_name + "/thresholding_" + thr + "_train_deep_features.hkl")
    else:
        raise TypeError
    true_indices = hkl.load("pipa_" + pipa_phase + "/" + part_name + "/thresholding_" + thr + "_indices_reverse.hkl")
    data, label = refine_data(data=data, true_indices=true_indices, false_per_true=false_per_true)
    total_data += data.tolist()
    total_label += label
    assert (len(total_data) == len(total_label))
    print len(total_data)

    # for idx, sample in enumerate(np.array(train_data)[np.delete(np.arange(len(train_data)), true_indices)]):
    #     shutil.copyfile(sample, sample_dir + "/pipa_test/" + str(idx) + ".jpg")
    # for idx, sample in enumerate(np.array(train_data)[true_indices]):
    #     shutil.copyfile(sample, sample_dir + "/pipa_test/" + str(idx) + ".jpg")

    return total_data, total_label


def refine_data(data, true_indices, false_per_true=1):
    """
    abandon true indeces, to match false_per_true rate
    zero for default
    :param data:
    :param true_indices:
    :param false_per_true:
    :return:
    """
    # generate data
    false_indices = np.delete(np.arange(len(data)), true_indices)
    if false_per_true != 0:
        false_indices = false_indices[: false_per_true * len(true_indices)]
    indices = np.append(false_indices, true_indices)

    # generate label
    label = []
    for idx in range(0, len(false_indices)):
        label.append(0)
    for idx in range(0, len(true_indices)):
        label.append(1)
    return np.array(data)[indices], label


def generate_lmdb(part_name=PartInformation.part_head_vgg_only):

    train_data, train_label, test_data, test_label = prepare_dataset_train_test(data_type=0, part_name=part_name)
    assert (len(train_data) == len(train_label))
    assert (len(test_data) == len(test_label))

    def generate(data, label, dbimages_dir, label_path, db_path, dbmean_dir, mean_name):
        with open(label_path, "w") as f:
            for idx, file_name in enumerate(data):
                f.write('%s %s\n' % (file_name, label[idx]))
        postproc.post_proc(images_dir=dbimages_dir,
                           label_path=label_path,
                           db_path=db_path,
                           mean_dir=dbmean_dir, mean_name=mean_name,
                           width=224, height=224
                           )

    # saving lmdb according to pipa_phase
    # pipa_test indices = > train
    # pipa_val indices = > test
    generate(data=train_data,
             label=train_label,
             dbimages_dir=images_dir + "/test/" + part_name,
             label_path=labels_dir + "/test/hard_negative_" + part_name + ".txt",
             db_path=labels_dir + "/test/hard_negative_vgg_only_lmdb",
             dbmean_dir=mean_dir + '/test',
             mean_name="hard_negative_" + part_name + "_mean")
    generate(data=test_data,
             label=test_label,
             dbimages_dir=images_dir + "/val/" + part_name,
             label_path=labels_dir + "/val/hard_negative_" + part_name + ".txt",
             db_path=labels_dir + "/val/hard_negative_vgg_only_lmdb",
             dbmean_dir=mean_dir + '/val',
             mean_name="hard_negative_" + part_name + "_mean")


def train_alex():

    pass


def train_svm(part_name=PartInformation.part_head_vgg_only):

    train_data, train_label, test_data, test_label = prepare_dataset_train_test(part_name=part_name, data_type=1)

    # train svm
    print "\n==> fitting svm"

    # grid search SVC
    param_grid = [
        {'C': [1, 10, 100, 1000],
         'kernel': ['linear']},
        {'C': [1, 10, 100, 1000],
         'gamma': [0.001, 0.0001],
         'kernel': ['rbf']}
    ]
    # clf = GridSearchCV(SVC(C=1, probability=True), param_grid, cv=5)

    # linear SVC
    # clf = LinearSVC(C=1.0)

    # rbf SVM
    clf = SVC(C=10, cache_size=200, class_weight=None, coef0=0.0,
              decision_function_shape=None, degree=3, gamma=0.001, kernel='rbf',
              max_iter=-1, probability=True, random_state=None, shrinking=True,
              tol=0.001, verbose=False)

    clf.fit(train_data, train_label)

    joblib.dump(clf, 'hard_negative_classifier_' + part_name + '.pkl')
    # clf = joblib.load('hard_negative_classifier_' + part_name + '.pkl')

    # predict and testing
    print "\n==> predicting and writing"
    predicted_labels = clf.predict(test_data)
    hit_1 = 0
    miss_1 = 0
    hit_2 = 0
    miss_2 = 0
    for idx, label in enumerate(test_label):
        # print label, predicted_labels[idx]

        if label == 0:
            if label == predicted_labels[idx]:
                hit_1 += 1
            else:
                miss_1 += 1
        elif label == 1:
            if label == predicted_labels[idx]:
                hit_2 += 1
            else:
                miss_2 += 1
        else:
            raise Exception

    print "type 1 score [important]"
    type1_score = Score(hit_1, miss_1, hit_1 + miss_1)
    type1_score.print_accuracy()
    type1_score.print_str()

    print "type 2 score"
    type2_score = Score(hit_2, miss_2, hit_2 + miss_2)
    type2_score.print_accuracy()
    type2_score.print_str()

    print clf.get_params()
    with open("hyper_parameter_tuning.txt", "a") as fw:
        fw.write(strftime("\n%Y-%m-%d %H:%M:%S\n"))
        fw.write("estimator : %s\n" % str(clf.best_estimator_))
        fw.write("params : %s\n" % str(clf.best_params_))
        fw.write("score : %s\n" % str(clf.best_score_))
        fw.write("\ntype 1 : %s" % type1_score.get_accuracy())
        fw.write("\ntype 2 : %s\n" % type2_score.get_accuracy())


result_dir = '/media/shlee/mipal_shlee_1tb/pipa_recognition'
# sample_dir = result_dir + '/hard_negative_samples'
sample_dir = result_dir + '/positive_samples'
features_dir = result_dir + '/features'
images_dir = result_dir + '/images'
labels_dir = result_dir + '/labels'
mean_dir = result_dir + '/mean'

if __name__ == "__main__":

    # generate_lmdb()
    train_svm()





