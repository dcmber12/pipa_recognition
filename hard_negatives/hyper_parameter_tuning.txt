2016-12-29 15:26:53
{'loss': 'squared_hinge', 'C': 1.0, 'verbose': 0, 'intercept_scaling': 1, 'fit_intercept': True, 'max_iter': 1000, 'penalty': 'l2', 'multi_class': 'ovr', 'random_state': None, 'dual': True, 'tol': 0.0001, 'class_weight': None}
type 1 : 84.8442906574
type 2 : 34.375

2016-12-29 16:33:30
{'n_jobs': 1, 'verbose': 0, 'estimator__gamma': 'auto', 'estimator__decision_function_shape': None,
 'estimator__probability': True,
  'param_grid': [{'kernel': ['linear'], 'C': [1, 10, 100, 1000]},
   {'kernel': ['rbf'], 'C': [1, 10, 100, 1000], 'gamma': [0.001, 0.0001]}],
   'cv': 5, 'scoring': None, 'estimator__cache_size': 200, 'estimator__verbose': False, 'pre_dispatch': '2*n_jobs',
    'estimator__kernel': 'rbf',
     'fit_params': {},
     'estimator__max_iter': -1, 'refit': True, 'iid': True, 'estimator__shrinking': True,
      'estimator__degree': 3,
       'estimator__class_weight': None,
        'estimator__C': 1, 'estimator__random_state': None,
       'return_train_score': True,
        'estimator': SVC(C=1, cache_size=200, class_weight=None, coef0=0.0,
  decision_function_shape=None, degree=3, gamma='auto', kernel='rbf',
  max_iter=-1, probability=True, random_state=None, shrinking=True,
  tol=0.001, verbose=False),
   'estimator__coef0': 0.0, 'error_score': 'raise', 'estimator__tol': 0.001}
type 1 : 98.2352941176
type 2 : 7.5

2016-12-29 17:11:20
{'kernel': 'rbf', 'C': 1, 'verbose': False, 'probability': True, 'degree': 3, 'shrinking': True, 'max_iter': -1, 'decision_function_shape': None, 'random_state': None, 'tol': 0.001, 'cache_size': 200, 'coef0': 0.0, 'gamma': 'auto', 'class_weight': None}
type 1 : 100.0
type 2 : 0.0

2016-12-29 17:45:59
SVC(C=10, cache_size=200, class_weight=None, coef0=0.0,
  decision_function_shape=None, degree=3, gamma='auto', kernel='linear',
  max_iter=-1, probability=True, random_state=None, shrinking=True,
  tol=0.001, verbose=False)
{'kernel': 'linear', 'C': 10}

type 1 : 84.5266272189
type 2 : 36.5079365079

2016-12-29 18:51:26
estimator : SVC(C=1000, cache_size=200, class_weight=None, coef0=0.0,
  decision_function_shape=None, degree=3, gamma=0.001, kernel='rbf',
  max_iter=-1, probability=True, random_state=None, shrinking=True,
  tol=0.001, verbose=False)
params : {'kernel': 'rbf', 'C': 1000, 'gamma': 0.001}
score : 0.807291666667

type 1 : 21.164021164
type 2 : 69.8412698413

2016-12-29 20:01:41
estimator : SVC(C=10, cache_size=200, class_weight=None, coef0=0.0,
  decision_function_shape=None, degree=3, gamma=0.001, kernel='rbf',
  max_iter=-1, probability=True, random_state=None, shrinking=True,
  tol=0.001, verbose=False)
params : {'kernel': 'rbf', 'C': 10, 'gamma': 0.001}
score : 0.861721068249

type 1 : 95.2366863905
type 2 : 15.3439153439

2016-12-29 20:57:32
