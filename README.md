### PIPA recognition ###

* pipa recognition implements by caffe and python

* uses prepare_data.py to
- separte face, body, parts, etc images
- create lmdb, mean, etc
- extract feature(AlexNet, VGG)

* uses train_test_predict.py to
- train svm and test
