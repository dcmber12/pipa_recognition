from sklearn.svm import SVC
from sklearn.svm import LinearSVC
import hickle as hkl
import prepare_data as pd
from const import PartInformation
import os
import time
import numpy as np
from label_util import LabelUtil


def train_weight(features, label):
    # features = hkl.load(pweight_dir + "/pweight_feature.hkl")
    # label = hkl.load(pweight_dir + "/pweight_label.hkl")
    clf = LinearSVC(C=1.0)
    clf.fit(features, label)
    coeff = clf.coef_
    hkl.dump(coeff, 'coeff.hkl')
    print "coeff : ",  coeff


def prepare_features():
    """
    prepare features for training weight : Pi(y|x)
    :return:
    """
    part_list = PartInformation.part_list()
    prob_table = hkl.load(prob_dir + '/' + "proba_head.hkl")

    # mapping sequential number to actual class name
    mapping = hkl.load(prob_dir + '/' + "proba_label_mapping.hkl")

    # actual class name for instance(sample)
    label_actual = hkl.load(prob_dir + '/' + "proba_labels.hkl")

    print type(prob_table[0][0])
    print type(label_actual[0])
    ninstance = prob_table.shape[0]
    nclas = prob_table.shape[1]
    print "prob table : ", prob_table.shape
    print "ninstance : ", ninstance
    print "nclass : ", nclas
    print "nTotal : ", ninstance * nclas

    prob_table = dict()
    for part in part_list:
        prob_table[part] = hkl.load(prob_dir + '/' + "proba_" + part + ".hkl")

    labels = []
    features = []  # len() : n_instance, len(item) : n_parts
    for instance in range(0, ninstance):
        for clas in range(0, nclas):
            # append features for item
            part_features = []
            for part in part_list:
                part_prob_table = prob_table[part]
                prob = part_prob_table[instance][clas]
                part_features.append(prob)
            features.append(part_features)
            # append labels for item
            if mapping[clas] == label_actual[instance]:
                labels.append(1)
            else:
                labels.append(-1)

        if (instance + 1) % 100 == 0:
            print "processed", instance + 1

    # print "dumping hkl..."
    # hkl.dump(features, pweight_dir + "/pweight_feature.hkl")
    # hkl.dump(labels, pweight_dir + "/pweight_label.hkl")
    # print "dumping done."
    print "features : ", len(features)
    print "labels : ", len(labels)
    train_weight(features=features, label=labels)


def prepare_probability_one_subset(phase="val"):

    clsmap = None
    for part in PartInformation.part_list():

        start = time.time()
        train_labels = hkl.load(pd.labels_dir + '/' + phase + '/' + 'train_labels.hkl')
        test_labels = hkl.load(pd.labels_dir + '/' + phase + '/' + 'test_labels.hkl')

        train_features = hkl.load(pd.features_dir + '/' + phase + '/' + 'features_' + part + '_train.hkl')
        test_features = hkl.load(pd.features_dir + '/' + phase + '/' + 'features_' + part + '_test.hkl')

        train_features, train_labels = LabelUtil.drop_out_feature(LabelUtil, train_features, train_labels)
        test_features, test_labels = LabelUtil.drop_out_feature(LabelUtil, test_features, test_labels)
        if len(list(set(train_labels))) < len(list(set(test_labels))):
            train_features, test_features = test_features, train_features
            train_labels, test_labels = test_labels, train_labels

        proba, classes_mapping = prepare_probabilty(features=train_features, labels=train_labels, tests=test_features)
        hkl.dump(proba, prob_dir + '/' + "proba_" + part + ".hkl")
        if clsmap is not None:
            list_identical(clsmap, classes_mapping)
        clsmap = classes_mapping
        end = time.time()
        print "svm for [%s] trained after %d seconds. " % (part, end - start)
        print "proba size: ", proba.shape

    # mapping sequential number to actual class name
    hkl.dump(clsmap, prob_dir + '/' + "proba_label_mapping.hkl")

    # list of actual class names
    hkl.dump(test_labels, prob_dir + '/' + "proba_labels.hkl")


def prepare_probability_vice_versa(phase="val"):
    # load validation data
    labels_first = hkl.load(pd.labels_dir + '/' + phase + '/' + 'train_labels.hkl')
    labels_second = hkl.load(pd.labels_dir + '/' + phase + '/' + 'test_labels.hkl')
    clsmap = None
    for part in [PartInformation.part_head, PartInformation.part_upper_body]:
        concat_proba, classes_mapping = prepare_probability_vice_versa_part(labels_first=labels_first, labels_second=labels_second, part=part)
        hkl.dump(concat_proba, prob_dir + '/' + "proba_" + part + ".hkl")
        if clsmap is not None:
            list_identical(clsmap, classes_mapping)
        clsmap = classes_mapping
        print "concat proba size: ", concat_proba.shape

    # mapping sequential number to actual class name
    hkl.dump(clsmap, prob_dir + '/' + "proba_label_mapping.hkl")

    # list of actual class names
    labels = labels_second.tolist() + labels_first.tolist()
    hkl.dump(labels, prob_dir + '/' + "proba_labels.hkl")


def prepare_probability_vice_versa_part(labels_first, labels_second, part, phase="val"):
    features = hkl.load(pd.features_dir + '/' + phase + '/' + 'features_' + part + '_train.hkl')
    tests = hkl.load(pd.features_dir + '/' + phase + '/' + 'features_' + part + '_test.hkl')
    print "==> First subset "
    features, labels = LabelUtil.drop_out_feature(origin_features=features, origin_labels=labels_first)
    proba_first_subset, classes_first = prepare_probabilty(features=features, labels=labels_first, tests=tests)
    print "first subset size : ", proba_first_subset.shape

    features = hkl.load(pd.features_dir + '/' + phase + '/' + 'features_' + part + '_test.hkl')
    tests = hkl.load(pd.features_dir + '/' + phase + '/' + 'features_' + part + '_train.hkl')
    print "==> Second subset"
    features, labels = LabelUtil.drop_out_feature(origin_features=features, origin_labels=labels)
    proba_second_subset, classes_second = prepare_probabilty(features=features, labels=labels_second, tests=tests)
    print "second subset size : ", proba_second_subset.shape

    list_identical(classes_first, classes_second)

    # concatenate two by 1st-dim
    return np.append(proba_first_subset, proba_second_subset, axis=0), classes_first


def list_identical(list_1, list_2):
    assert len(list_1) == len(list_2)
    for idx, clas in enumerate(list_1):
        if clas != list_2[idx]:
            raise Exception


def prepare_probabilty(features, labels, tests):
    assert len(features) == len(labels)
    print "instance size : ", len(labels)
    print "label size : ", len(set(labels))

    print "==> fitting svm"
    clf = SVC(C=1.0, kernel="linear", probability=True)
    clf.fit(features, labels)
    return clf.predict_proba(tests), clf.classes_


prob_dir = pd.result_dir + "/prob_table"
if __name__ == "__main__":

    # prepare data
    # prepare_probability_vice_versa()
    prepare_probability_one_subset()
    prepare_features()



