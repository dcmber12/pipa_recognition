import os
import numpy as np
import caffe
import subprocess
import shutil

CAFFE_DIR = '/home/shlee/caffe-master'


def post_proc(images_dir, label_path, db_path, mean_dir, mean_name, width=227, height=227):
    """
    post process for images before using caffe
    :param images_dir:
    :param label_path:
    :param db_path: path to generate db
    :param mean_dir: path to compute mean
    :param mean_name: name of mean file
    :param width:
    :param height:
    :return:
    """
    # convert imageset
    if os.path.isdir(db_path):
        shutil.rmtree(db_path)
    convert_imageset(images_dir + '/', label_path, db_path, width, height)

    # compute image mean
    mean = compute_mean(db_path, mean_dir, mean_name)

    # convert mean
    convert_mean(mean)


def convert_imageset(images_dir, label_path, result, width=227, height=227):
    subprocess.check_call([CAFFE_DIR + '/build/tools/convert_imageset',
                           # resize
                           '--resize_width', str(width), '--resize_height', str(height),
                           # images dir
                           images_dir,
                           # labels
                           label_path,
                           # output
                           result])


def compute_mean(db_path, out_path, out_name):
    if not os.path.isdir(out_path):
        os.makedirs(out_path)
    output = out_path + "/" + out_name + ".binaryproto"
    subprocess.check_call([CAFFE_DIR + '/build/tools/compute_image_mean', db_path, output])
    print "computing mean done."
    return output


def convert_mean(input_binaryproto):
    data = open(input_binaryproto, "rb").read()

    blob = caffe.proto.caffe_pb2.BlobProto()
    blob.ParseFromString(data)

    arr = np.asarray(caffe.io.blobproto_to_array(blob))
    print "converting mean done."
    print arr.shape

    name, _ = os.path.splitext(input_binaryproto)

    np.save(name + ".npy", arr)


if __name__ == "__main__":
    # db = ""
    # out_dir = ""
    # mean_name = ""
    # compute_mean(db, out_dir, name)
    pass

