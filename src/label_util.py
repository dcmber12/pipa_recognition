class LabelUtil:

    def __init__(self):
        pass

    @classmethod
    def drop_out_labels(cls, origin_labels):

        # collect drop list(too small number of sample)
        origin_labels = origin_labels.tolist()  # numpy to list
        unique_label = list(set(origin_labels))  # collect unique list
        drop_list = [label for label in unique_label if origin_labels.count(label) < LabelUtil.get_thresh()]
        print 'drop list size : ', len(drop_list)

        # drop out
        result_labels = []
        for idx, label in enumerate(origin_labels):
            if label in drop_list:
                continue
            result_labels.append(label)

        unique_label_result = list(set(result_labels))
        print "==> label thresholding : ", len(unique_label), " => ", len(unique_label_result)
        return result_labels

    @classmethod
    def drop_out_feature(cls, origin_features, origin_labels):
        """
        thresholding and throw away from original features list and labels list, by number of class
        :return: tuple of (features, labels) that are reduced
        """

        # collect drop list(too small number of sample)
        origin_labels = origin_labels.tolist()  # numpy to list
        unique_label = list(set(origin_labels))  # collect unique list
        drop_list = [label for label in unique_label if origin_labels.count(label) < LabelUtil.get_thresh()]
        print 'drop list size : ', len(drop_list)

        # drop out
        result_features = []
        result_labels = []
        for idx, label in enumerate(origin_labels):
            if label in drop_list:
                continue
            result_labels.append(label)
            result_features.append(origin_features[idx])

        unique_label_result = list(set(result_labels))
        print "==> label thresholding : ", len(unique_label), " => ", len(unique_label_result)
        print "feature size : ", len(result_features)
        assert len(result_features) == len(result_labels)
        return result_features, result_labels

    @staticmethod
    def get_thresh():
        # if thresholding 25 : sample sizes squeezes into 1686(train) and 1684(test) class 581 -> 45
        return 25
