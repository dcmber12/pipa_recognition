class Score:

    def __init__(self, hit, miss, total):
        self.hit = hit
        self.miss = miss
        self.total = total

    def get_print_str(self):
        return '[hit :  %s ] [miss : %s ] [total : %s ]\n\n' % (self.hit, self.miss, self.total)

    def print_str(self):
        print self.get_print_str()

    def get_accuracy(self):
        return (self.hit * 100.0) / (self.hit + self.miss)

    def print_accuracy(self):
        accuracy = self.get_accuracy()
        print "accuracy : ", "{:.4f}".format(accuracy)



