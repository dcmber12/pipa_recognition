

def check(phase):
    label_name = labels_dir + "/" + phase + "/" + part + ".txt"
    with open(label_name, 'r') as l:
        labels = l.readlines()

    lines = [line.rstrip() for line in labels]
    labels = [line.split(" ")[1] for line in lines]

    labels = list(set(labels))
    print phase + " length : " + str(len(labels))

if __name__ == "__main__":

    # location to PIPA dataset
    root = '/media/shlee/mipal_shlee_1tb/PIPA'

    # location to save features
    result_dir = '/media/shlee/mipal_shlee_1tb/pipa_recognition'
    images_dir = result_dir + '/images'
    models_dir = result_dir + '/model'
    weight_dir = result_dir + '/weight'
    mean_dir = result_dir + '/mean'
    features_dir = result_dir + '/features'
    labels_dir = result_dir + '/labels'

    part = "head"
    # check class number
    check("train")
    check("test")
    check("val")






