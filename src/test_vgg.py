# import feature_extract as fe
# import caffe
# from const import PartInformation
# import hickle as hkl
# import prepare_data as pd
# import cv2
#
#
# def extract_feature_vgg():
#
#     frontal_net = fe.CaffeFeatureExtractor(
#         model_path=pd.models_dir + "/VGG_FACE_deploy.prototxt",
#         pretrained_path=pd.weight_dir + "/VGG_FACE.caffemodel",
#         blob="fc7",
#         crop_size=227,
#         meanfile_path=pd.mean_dir + "/head_mean.npy"
#     )
#
#     train_label = hkl.load(pd.labels_dir + "/train_labels.hkl")
#     test_label = hkl.load(pd.labels_dir + "/train_labels.hkl")
#
#     frontal_train_feat, frontal_train_label = hkl.load("vgg_train_feats.hkl")
#     if frontal_train_feat is None or frontal_train_label is None:
#         frontal_train_feat, frontal_train_label = front_feature(frontal_net, train_label)
#         hkl.dump(frontal_train_feat, "vgg_test_feats.hkl")
#         hkl.dump(frontal_train_feat, "vgg_test_feats.hkl")
#
#     frontal_test_feat, frontal_test_label = hkl.load("vgg_test_feats.hkl")
#     if frontal_test_feat is None or frontal_test_label is None:
#         frontal_test_feat, frontal_test_label = front_feature(frontal_net, test_label)
#
#
# def front_feature(net, label):
#     feat_list = []
#     num_frontal = 0
#     num_non_frontal = 0
#     for idx, data in enumerate(label):
#         data = pd.images_dir + "/" + data
#         bgr_img = cv2.imread(data)
#         if bgr_img is None:
#             raise Exception("Unable to load image: {}".format(data))
#
#         rgb_img = cv2.cvtColor(bgr_img, cv2.COLOR_BGR2RGB)
#         aligned_face = pd.align.align(
#             imgDim=96,
#             rgbImg=rgb_img,
#             landmarkIndices=pd.openface.AlignDlib.OUTER_EYES_AND_NOSE)
#
#         if aligned_face is None:
#             num_non_frontal += 1
#             continue
#         else:
#             feat = pd.extract_features(net=net, data=data)
#             num_frontal += 1
#
#         if (idx + 1) % 100 == 0:
#             print "processed %s : %d" % ("vgg head", idx + 1)
#         feat_list.append(feat)
#
#     print "extract head(VGG) done "
#     print '[frontal : %d ] [non_frontal : %d ] [total : %d ]\n' % \
#           (num_frontal, num_non_frontal, num_frontal + num_non_frontal)
#
#     return feat_list
#
#
# def test_svm():
#     # train_label =
#
#
#
