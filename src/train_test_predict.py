# -*- coding: utf-8 -*-

import numpy as np
import hickle as hkl
import pickle as pkl
import cPickle as ckl
from const import PartInformation
from sklearn.svm import LinearSVC
from sklearn.svm import SVC
from time import localtime, strftime, time
import prepare_data
from label_util import LabelUtil
import scipy.stats as stats
import pylab as pl
from data_model import Score
from sklearn.externals import joblib


def run(part):

    # part = PartInformation.part_head_vgg_only
    # part = PartInformation.part_head
    # part = PartInformation.part_concat

    # Training SVM
    # part = PartInformation.part_head_vgg_only
    print "\n==> running ..... [", part, "]  th : ", LabelUtil.get_thresh()
    train_features, train_labels, test_features, test_labels = prepare_part_data(part)
    # hkl.dump(train_features, '../hard_negatives/pipa_' + pipa_phase + '/' + part + '/thresholding_' + str(
    #     LabelUtil.get_thresh()) + '_train_deep_features.hkl')
    # hkl.dump(test_features, '../hard_negatives/pipa_' + pipa_phase + '/' + part + '/thresholding_' + str(
    #     LabelUtil.get_thresh()) + '_test_deep_features.hkl')

    clf = classification(train_features=train_features,
                   train_labels=train_labels)

    score, hard_negatives_indices = predict(clf=clf,
                                       test_features=test_features,
                                       test_labels=test_labels,
                                       part_name=part,
                                       part_number=len(train_features[0]) / 4096)
    # hkl.dump(hard_negatives_indices, '../hard_negatives/pipa_' + pipa_phase + '/' + part + '/thresholding_' + str(LabelUtil.get_thresh()) + '_indices.hkl')
    score.print_accuracy()
    score.print_str()

    ########################
    # Reverse test
    ########################
    # clf = classification(train_features=test_features,
    #                      train_labels=test_labels)
    #
    # _, hard_negatives_indices = predict(clf=clf,
    #                                    test_features=train_features,
    #                                    test_labels=train_labels,
    #                                    part_name=part,
    #                                    part_number=len(train_features[0]) / 4096)
    # hkl.dump(hard_negatives_indices, '../hard_negatives/pipa_' + pipa_phase + '/' + part + '/thresholding_' + str(LabelUtil.get_thresh()) + '_indices_reverse.hkl')

    ########################
    # For divide and conquer IDEAL
    ########################
    # total_score = Score(score.hit, "%s < " % score.miss, score.total)
    #
    # part = PartInformation.part_upper_body
    # print "running ..... [", part, "]  th : ", str(LabelUtil.get_thresh())
    # train_features, train_labels, test_features, test_labels = prepare_part_data(part)
    # clf = classification(train_features=train_features,
    #                      train_labels=train_labels,
    #                      )
    #
    # hard_negative_feat = [test_features[i] for i in hard_negatives_indices]
    # hard_negative_label = [test_labels[i] for i in hard_negatives_indices]
    # score, hard_negatives_indices = predict(clf=clf,
    #                                     test_features=hard_negative_feat,
    #                                     test_labels=hard_negative_label,
    #                                     part_name=part,
    #                                     part_number=len(train_features[0]) / 4096)
    #
    # total_score = Score(total_score.hit + score.hit, score.miss, total_score.total)
    # print "<total score>"
    # print "accuracy : ", "{:.4f}".format((total_score.hit * 100.0) / (total_score.hit + total_score.miss))
    # print total_score.get_print_str()

    # Weighted sum SVM
    # probability(PartInformation.part_list())
    # predict_svm_probability([PartInformation.part_vgg_only, PartInformation.part_upper_body, PartInformation.part_body])

    # prepare_coeff_features([PartInformation.part_vgg_only, PartInformation.part_upper_body, PartInformation.part_body])

    # test_probability([PartInformation.part_vgg_only, PartInformation.part_upper_body, PartInformation.part_body])


def divide_and_conquer():

    ##########################
    # For divide and conquer
    ##########################

    part = PartInformation.part_head_vgg_only
    hn_clf = joblib.load('hard_negative_classifier_' + part + '.pkl')
    train_features, train_labels, head_test_features, test_labels = prepare_part_data(part)
    head_clf = classification(train_features=train_features, train_labels=train_labels)

    part = PartInformation.part_upper_body
    train_features, train_labels, body_test_features, test_labels = prepare_part_data(part)
    body_clf = classification(train_features=train_features, train_labels=train_labels)

    hit = 0
    miss = 0
    divide_list = hn_clf.predict(head_test_features)
    for idx, divide in enumerate(divide_list):
        if divide == 0:
            prediction = head_clf.predict(head_test_features[idx].reshape(1, -1))
        elif divide == 1:
            prediction = body_clf.predict(body_test_features[idx].reshape(1, -1))
        else:
            raise Exception
        if prediction == test_labels[idx]:
            hit += 1
        else:
            miss += 1

    score = Score(hit=hit, miss=miss, total=hit + miss)
    score.print_str()
    score.print_accuracy()


def predict_svm_probability(part_list):
    """
    predict svm probability (instance X class) table(np)
    :param part_list:
    :return:
    """
    class_mapping = None
    clf = SVC(C=1, kernel='linear', probability=True)
    for idx, part in enumerate(part_list):
        data = prepare_part_data(part)
        print "\n", strftime("%Y-%m-%d %H:%M:%S")
        print "training %s svm ..." % part
        start = time()
        clf.fit(data.train_features, data.train_labels)
        proba = clf.predict_proba(data.test_features)
        if idx != 0:
            list_identical(class_mapping, clf.classes_)
        class_mapping = clf.classes_
        hkl.dump(proba, "svm_proba_" + part + ".hkl")
        end = time()
        print "training %s took %d seconds\n" % (part, end - start)

    hkl.dump(class_mapping, "cls_mapping.hkl")


def prepare_coeff_features(part_list):
    """
    prepare and training weight : [wi] * Pi(y|x)
    :return:
    """

    # mapping sequential number to actual class name
    mapping = hkl.load("cls_mapping.hkl")

    # actual class name for instance(sample)
    test_labels = get_test_labels()

    ninstance = len(test_labels)
    nclas = len(list(set(test_labels)))

    print "ninstance : ", ninstance
    print "nclass : ", nclas
    print "nTotal : ", ninstance * nclas

    prob_table = dict()
    for part in part_list:
        prob_table[part] = hkl.load("svm_proba_" + part + ".hkl")

    labels = []
    features = []  # len() : n_instance, len(item) : n_parts
    for instance in range(0, ninstance):
        for clas in range(0, nclas):
            # append features for item
            part_features = []
            for part in part_list:
                part_prob_table = prob_table[part]
                prob = part_prob_table[instance][clas]
                part_features.append(prob)
            features.append(part_features)
            # append labels for item
            if mapping[clas] == test_labels[instance]:
                labels.append(1)
            else:
                labels.append(-1)

        if (instance + 1) % 100 == 0:
            print "processed", instance + 1

    # print "dumping hkl..."
    # hkl.dump(features, pweight_dir + "/pweight_feature.hkl")
    # hkl.dump(labels, pweight_dir + "/pweight_label.hkl")
    # print "dumping done."
    print "features : ", len(features)
    print "labels : ", len(labels)
    train_coeff(features=features, label=labels)


def train_coeff(features, label):
    # features = hkl.load(pweight_dir + "/pweight_feature.hkl")
    # label = hkl.load(pweight_dir + "/pweight_label.hkl")
    clf = LinearSVC(C=1.0)
    clf.fit(features, label)
    coeff = clf.coef_
    hkl.dump(coeff, 'coeff.hkl')
    print "coeff : ",  coeff


def test_probability(part_list):

    prob_dict = dict()
    class_mapping = hkl.load("cls_mapping.hkl")
    for part in part_list:
        proba = hkl.load("svm_proba_" + part + ".hkl")
        prob_dict[part] = proba

    # MAX POOLING
    # max_proba = np.maximum([prob_dict[part] for part in part_list])
    # max_proba = np.maximum(hkl.load("svm_proba_vgg_only.hkl"),
    #                        hkl.load("svm_proba_upper_body.hkl"),
    #                        hkl.load("svm_proba_body.hkl"))
    max_proba = hkl.load("svm_proba_vgg_only.hkl")

    # WEIGHTED SUM
    # coeff = hkl.load("coeff.hkl").tolist()
    # weighted_proba = np.maximum([prob_dict[part] * coeff[0][idx] for idx, part in enumerate(part_list)])

    hit = 0
    miss = 0
    print "training all parts done."
    predict_list = np.argmax(max_proba, axis=1)
    test_label = get_test_labels()
    print "---------"
    print len(test_label)
    print predict_list.shape
    print "---------"
    for idx, predict in enumerate(predict_list):
        predict = class_mapping[predict]
        label = test_label[idx]
        if predict == label:
            hit += 1
        else:
            miss += 1

    write_result(hit=hit, miss=miss, part_name="concat(probability)", part_num=len(part_list))


def list_identical(list_1, list_2):
    assert len(list_1) == len(list_2)
    for idx, clas in enumerate(list_1):
        if clas != list_2[idx]:
            raise Exception


def classification(train_features, train_labels):

    clf = LinearSVC(C=1.0)
    # clf = SVC(C=1, kernel='linear', probability=True)

    # param_grid = [
    #     {'C': [1, 10, 100, 1000],
    #      'kernel': ['linear']},
    #     # {'C': [1, 10, 100, 1000],
    #     #  'gamma': [0.001, 0.0001],
    #     #  'kernel': ['rbf']}
    # ]
    # clf = GridSearchCV(svm.SVC(C=1, probability=True), param_grid, cv=3)

    # print "==> training and test"
    # X_train = train_features[-1000:]
    # T_train = train_labels[-1000:]
    # X_test = train_features[:-1000]
    # T_test = train_labels[:-1000]
    # svm.fit(X_train, T_train)
    # Y_test = svm.predict(X_test)
    # print confusion_matrix(T_test, Y_test)
    # print accuracy_score(T_test, Y_test)
    # print classification_report(T_test, Y_test)

    # 10 times cross validate
    # print "==> cross validation"
    # scores = cross_val_score(svm, train_features, train_labels, cv=10)
    # print "Accuracy: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std())

    # libSVM
    # prob = svm_problem(train_labels, train_features)
    # param = svm_parameter('-t 0')
    # m = svm_train(prob, param)
    # p_label, p_acc, p_val = svm_predict(test_la, test_features, m)

    # train svm
    print "\n==> fitting svm"
    clf.fit(train_features, train_labels)
    return clf


def predict(clf, test_features, test_labels, part_name, part_number=None):

    # hard_negative_indices = hnclf.classify(test_features=test_features)
    # hkl.dump(hard_negative_indices, "../hard_negatives/" + part_name + "_hard_negative_indices.hkl")

    # predict and testing
    print "\n==> predicting and writing"
    predicted_labels = clf.predict(test_features)
    hit = 0
    miss = 0
    # positive_features = []
    # hard_negative_features = []
    hard_negative_indices = []
    for idx, test_label in enumerate(test_labels):
        if test_label == predicted_labels[idx]:
            hit += 1
            # positive_features.append(test_features[idx])
        else:
            miss += 1
            hard_negative_indices.append(idx)
            # hard_negative_features.append(test_features[idx])

    hkl.dump(hard_negative_indices, "../hard_negatives/" + part_name + "_hard_negative_indices.hkl")
    write_result(hit, miss, part_name=part_name, part_num=part_number)
    score = Score(hit, miss, hit + miss)
    with open(part_name + "_predict.txt", "w") as fw:
        fw.write(score.get_print_str())
        fw.write("id,label\n")
        for i in xrange(len(test_labels)):
            fw.write("%d,%d\n" % (test_labels[i], predicted_labels[i]))

    return score, hard_negative_indices


def write_result(hit, miss, part_name, part_num):
    accuracy = (hit * 100.0) / (hit + miss)
    print "accuracy : ", "{:.4f}".format(accuracy), " [", part_name, "]"
    score = Score(hit, miss, hit + miss)
    print score.get_print_str()
    with open("log.txt", "a") as fw:
        fw.write(strftime("%Y-%m-%d %H:%M:%S\n"))
        fw.write("phase : %s\n" % pipa_phase)
        fw.write("part : %s , part_num : %s\n" % (part_name, part_num))
        fw.write("{:.4f}\n\n".format(accuracy))


def get_test_labels():
    return LabelUtil.drop_out_labels(hkl.load(labels_phase_dir + '/' + "test_labels.hkl"))


def prepare_part_data(part_name, feat_type=0):
    """
    loads features list And label(both train and test) from hkl, thresholding and throw it by number of class
    :return: a dictionary contains
            key : {part_name} + {_phase} + {_features or _labels}
    """

    # loading train data
    # print "==> loading train data from %s" % (part_name + "_train_(features|labels).hkl")
    if feat_type == 0:
        train_features = hkl.load(features_dir + '/' + 'features_' + part_name + "_train.hkl")
    else:
        train_features = hkl.load(labels_phase_dir + "/train_file_names.hkl")
    train_labels = hkl.load(labels_phase_dir + '/' + "train_labels.hkl")
    # print "train_features length = ", len(train_features)
    train_features, train_labels = LabelUtil.drop_out_feature(train_features, train_labels)
    print "train_labels length = ", len(train_labels)
    assert len(train_features) == len(train_labels)

    # print "train_features size = ", len(train_features[0])
    # print "size / 4096 = ", len(train_features[0]) / 4096

    # loads test data
    # print "==> loading test data from %s" % (part_name + "_test_(features|labels).hkl")
    if feat_type == 0:
        test_features = hkl.load(features_dir + '/' + 'features_' + part_name + "_test.hkl")
    else:
        test_features = hkl.load(labels_phase_dir + "/test_file_names.hkl")
    test_labels = hkl.load(labels_phase_dir + '/' + "test_labels.hkl")
    # print "test_features length = ", len(test_features)
    test_features, test_labels = LabelUtil.drop_out_feature(test_features, test_labels)
    print "test_labels length = ", len(test_labels)
    assert len(test_features) == len(test_labels)

    print "average %d samples per class." % (len(train_labels) / len(list(set(train_labels))))

    # if len(list(set(train_labels))) < len(list(set(test_labels))):
    #     # con = input("test set has more labels. continue (y/n)")
    #     # if con != "y" and con != "Y":
    #     #     raise Exception
    #     print "converting train <-> test"
    #     return PartInformation(train_features=test_features,
    #                            train_labels=test_labels,
    #                            test_features=train_features,
    #                            test_labels=train_labels,
    #                            part_name=part_name)

    return train_features, train_labels, test_features, test_labels


def hist():
    test_labels = sorted(hkl.load(labels_phase_dir + '/' + "test_labels.hkl").tolist())
    classses = list(set(test_labels))
    appearance_list = []
    for cls in classses:
        appear = test_labels.count(cls)
        appearance_list.append(appear)
    pl.hist(appearance_list, bins=10, normed=True)
    pl.show()


if __name__ == "__main__":

    pipa_phase = "test"
    # pipa_phase = "val"

    result_dir = '/media/shlee/mipal_shlee_1tb/pipa_recognition'
    features_dir = result_dir + '/features/' + pipa_phase
    labels_phase_dir = result_dir + '/labels/' + pipa_phase

    # samples for hard negative analysis
    # print "sampling for hard negative analysis...."
    # part_name = PartInformation.part_upper_body
    # hn_train_features, hn_train_labels, hn_test_features, hn_test_labels = prepare_part_data(PartInformation.part_head_vgg_only, feat_type=1)
    # pkl.dump(hn_train_features, open('../hard_negatives/pipa_' + pipa_phase + '/' + part_name + '/thresholding_' + str(
    #     LabelUtil.get_thresh()) + '_train_file_names.p', 'w'))
    # hkl.dump(hn_train_labels, '../hard_negatives/pipa_' + pipa_phase + '/' + part_name + '/thresholding_' + str(
    #     LabelUtil.get_thresh()) + '_train_labels.hkl')
    # pkl.dump(hn_test_features, open('../hard_negatives/pipa_' + pipa_phase + '/' + part_name + '/thresholding_' + str(
    #     LabelUtil.get_thresh()) + '_test_file_names.p', 'w'))
    # hkl.dump(hn_test_labels, '../hard_negatives/pipa_' + pipa_phase + '/' + part_name + '/thresholding_' + str(
    #     LabelUtil.get_thresh()) + '_test_labels.hkl')

    # list of parts
    # run(PartInformation.part_head_vgg_only)
    # run(PartInformation.part_vgg_upper_body)
    run(PartInformation.part_vgg_body)
    # run(PartInformation.part_concat)
    # run(PartInformation.part_body)
    # run(PartInformation.part_head)
    # run(part_name)

    # hist()

    # divide_and_conquer()


