import numpy as np


def get_roi(original, xmin, ymin, proposed_width, proposed_height):
    origin_height, origin_width, _ = original.shape
    roi_xmin = max(xmin, 0)
    roi_ymin = max(ymin, 0)
    if roi_xmin >= origin_width or roi_ymin >= origin_height:
        print "this bounding box is not valid."
        print roi_xmin, roi_ymin
        return None

    roi_width = min(origin_width - roi_xmin, proposed_width)
    roi_height = min(origin_height - roi_ymin, proposed_height)
    if roi_width < 12 or roi_height < 12:
        print "this bounding box is too small."
        print roi_width, roi_height
        return None

    return original[roi_ymin:roi_ymin + roi_height, roi_xmin:roi_xmin + roi_width]
