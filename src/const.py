class PartInformation:

    """
    change part_list as your will
    """

    part_head = "head"
    part_head_vgg_only = "vgg_only"
    part_head_casia = "head_casia"
    part_head_cacd = "head_cacd"
    part_head_age = "head_age"
    part_head_gender = "head_gender"
    part_head_glasses = "head_glasses"
    part_head_haircolour = "head_haircolour"
    part_head_hairlength = "head_hairlength"

    part_upper_body = "upper_body"
    part_upper_body_age1 = "upper_body_age1"
    part_upper_body_age2 = "upper_body_age2"
    part_upper_body_gender = "upper_body_gender"
    part_upper_body_hairblack = "upper_body_hairblack"
    part_upper_body_hairshort = "upper_body_hairshort"

    part_body = "body"
    part_vgg_body = "vgg_body"
    part_vgg_upper_body = "vgg_upper_body"

    part_face = "face"
    part_scene = "scene"
    part_concat = "concat"

    @staticmethod
    def part_list():
        return [PartInformation.part_head,
                PartInformation.part_body,
                PartInformation.part_upper_body,
                # PartInformation.part_face,
                # PartInformation.part_scene
                ]

    @staticmethod
    def get_combine_name(part_list):
        if part_list is None or len(part_list) < 1:
            raise Exception
        combine = ""
        for idx, part in enumerate(part_list):
            if idx > 0:
                combine += "_and_"
            combine += part
        return combine

    def __init__(self, train_features, train_labels, test_features, test_labels, part_name):
        self.train_features = train_features
        self.train_labels = train_labels
        self.test_features = test_features
        self.test_labels = test_labels
        self.part_name = part_name




