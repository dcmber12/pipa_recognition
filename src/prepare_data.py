"""
prepare dataset

from PIPA test set,
iterate each images in test set and extract part features

"""
import os
import numpy as np
import cv2
import feature_extract as fe
import caffe
from const import PartInformation
import hickle as hkl
import roi
import db_postproc as postproc
import time
import openface
import random
import shutil


# location to PIPA dataset
root = '/media/shlee/mipal_shlee_1tb/PIPA'

# location to save features
result_dir = '/media/shlee/mipal_shlee_1tb/pipa_recognition'
images_dir = result_dir + '/images'
models_dir = result_dir + '/model'
weight_dir = result_dir + '/weight'
mean_dir = result_dir + '/mean'
features_dir = result_dir + '/features'
labels_dir = result_dir + '/labels'


def get_images(part_list, phase="train"):

    """
    extract images from annotaions
    and save separate directory
    with original file name
    > /head/photoset_photo.jpg
    :return:
    """
    # part directory to clean
    for part in part_list:
        name = images_dir + "/" + phase + "/" + part
        if not os.path.isdir(name):
            os.makedirs(name)
        else:
            pre_result = os.listdir(name)
            remove_cnt = 0
            for f in pre_result:
                if os.path.exists(name + "/" + f):
                    os.remove(name + "/" + f)
                    remove_cnt += 1
            print 'output dir is cleared : ', remove_cnt, ' file is deleted.'

    if phase == "leftover":
        subset_id = "0"
    elif phase == "train":
        subset_id = "1"
    elif phase == "val":
        subset_id = "2"
    elif phase == "test":
        subset_id = "3"
    else:
        raise Exception

    with open(root + "/annotations/index.txt", 'r') as f:
        lines = f.readlines()

    lines = [line.rstrip() for line in lines]

    # The subset IDs are 0 for leftover, 1 for train, 2 for validation, 3 for test
    num_skipped = 0
    dest_dir = images_dir + "/" + phase
    for idx, line in enumerate(lines):

        items = line.split(' ')
        if items[anno_subset_id] != subset_id:
            continue

        file_name = items[anno_photoset_id] + "_" + items[anno_photo_id] + ".jpg"
        path = root + "/" + phase + "/" + file_name
        bgr_img = cv2.imread(path)
        if bgr_img is None:
            raise Exception
        # rgb_img = cv2.cvtColor(bgr_img, cv2.COLOR_BGR2RGB)
        height, width, _ = bgr_img.shape

        # get head bounding box
        head = roi.get_roi(bgr_img,
                           int(items[anno_xmin]), int(items[anno_ymin]),
                           int(items[anno_width]), int(items[anno_height]))
        if head is None:
            print path, "\n"
            num_skipped += 1
            continue

        # get Body bounding box

        # 3 X 6 BODY REGION (Oh et al.)
        # bxmin = max(int(items[anno_xmin]) - ((WIDTH_SCALE - 1) * int(items[anno_width])) / 2, 0)
        # bymin = max(int(items[anno_ymin]), 0)
        # bwidth = 3 * int(items[anno_width])
        # bheight = 6 * int(items[anno_height])

        # Dynamic 2 X 6 BODY REGION (Haoxiang Li et al.)
        length = min(int(items[anno_width]), int(items[anno_height]))
        bxmin = max(int(items[anno_xmin]) - int(0.5*length), 0)
        bymin = max(int(items[anno_ymin]), 0)
        bwidth = 2 * length
        bheight = 6 * length

        body = roi.get_roi(bgr_img, bxmin, bymin, bwidth, bheight)
        if body is None:
            print path, "\n"
            num_skipped += 1
            continue

        # get upper body bounding box
        bheight /= 2
        upper = roi.get_roi(bgr_img, bxmin, bymin, bwidth, bheight)
        if upper is None:
            print path, "\n"
            num_skipped += 1
            continue

        # !!Be warn!!, one image may has multiple instances so each instance must be separated.
        file_name, ext = os.path.splitext(file_name)
        file_name = file_name + "_" + items[anno_identity_id] + ext

        if PartInformation.part_head in part_list:
            cv2.imwrite(dest_dir + "/" + PartInformation.part_head + "/"
                        + file_name, head)
        if PartInformation.part_body in part_list:
            cv2.imwrite(dest_dir + "/" + PartInformation.part_body + "/"
                        + file_name, body)
        if PartInformation.part_upper_body in part_list:
            cv2.imwrite(dest_dir + "/" + PartInformation.part_upper_body + "/"
                        + file_name, upper)
        if PartInformation.part_scene in part_list:
            cv2.imwrite(dest_dir + "/" + PartInformation.part_scene + "/"
                        + file_name, bgr_img)
        if (idx + 1) % 100 == 0:
            print "processed", idx + 1

    for part in PartInformation.part_list():
        print part, " : ", len(os.listdir(dest_dir + "/" + part)), " images generated."
    print num_skipped, " images skipped."
    # print "num_leftover : ", num_leftover
    # print "num_train : ", num_train
    # print "num_val : ", num_val
    # print "num_test : ", num_test


def post_proc(part_list, phase="train"):
    """
    generate label for lmdb, make lmdb(leveldb), compute image mean
    :return:
    """

    for part in part_list:

        # generate text label for make db
        part_dir = images_dir + "/" + phase + "/" + part
        if not os.path.isdir(part_dir):
            raise Exception
        images = os.listdir(part_dir)
        """
        1) labels/phase/part.txt
        2) labels/phase/part_lmdb
        3) mean/phase/part_mean.{npy, binaryproto}

        """
        label_phase_dir = labels_dir + '/' + phase
        if not os.path.isdir(label_phase_dir):
            os.makedirs(label_phase_dir)

        label_name = label_phase_dir + "/" + part + ".txt"
        with open(label_name, 'w') as l:
            for image in images:
                name, ext = os.path.splitext(image)
                items = name.split('_')
                l.write('%s %s\n' % (image, items[2]))

        # post process
        print "==> post processing ", part
        postproc.post_proc(images_dir=part_dir,
                           label_path=label_name,
                           db_path=label_phase_dir + '/' + part + "_lmdb",
                           mean_dir=mean_dir + '/' + phase, mean_name=part + '_mean',
                           width=224,
                           height=224
                           )


def post_proc_combine_part(part_list):

    combine_name = PartInformation.get_combine_name(part_list)
    dest_dir = images_dir + "/train/" + combine_name
    cls_list = []
    max_cls_val = -1
    if not os.path.isdir(dest_dir):
        os.mkdir(dest_dir)

    """
    combine all part directory images into one directory
    """
    for pidx, part in enumerate(part_list):

        part_dir = images_dir + "/train/" + part
        if not os.path.isdir(part_dir):
            raise Exception
        images = os.listdir(part_dir)

        print "maximum class number : " + str(max_cls_val)
        if pidx == 0:
            print "copying %s ==> %s to %s" % (part, part_dir, dest_dir)
            for idx, image in enumerate(images):
                name, ext = os.path.splitext(image)
                items = name.split('_')
                cls_list.append(int(items[2]))
                shutil.copy(part_dir + '/' + image, dest_dir + '/' + image)
                if (idx + 1) % 1000 == 0:
                    print '[%s] processed %d' % (part, idx + 1)
            max_cls_val = max(cls_list)
        else:
            if max_cls_val <= 0:
                raise Exception
            for idx, image in enumerate(images):
                name, ext = os.path.splitext(image)
                items = name.split('_')
                # shutil.copy(part_dir + '/' + image, dest_dir + '/' + items[0] + '_' + items[1] + '_' + str(
                #     max_cls_val * pidx + int(items[2])) + ext)
                shutil.copy(part_dir + '/' + image, dest_dir + '/' + items[0] + '_' + items[1] + '_' +
                    items[2] + '_2' + ext)
                if (idx + 1) % 1000 == 0:
                    print '[%s] processed %d' % (part, idx + 1)

    post_proc_train_split([combine_name])


def post_proc_train_split(part_list):

    for part in part_list:

        part_dir = images_dir + "/train/" + part
        if not os.path.isdir(part_dir):
            raise Exception
        images = os.listdir(part_dir)

        # separates images into 8:2
        random.shuffle(images)

        num_train = int(len(images) * 0.8)
        train_images = images[:num_train]
        test_images = images[num_train:]

        def post_proc_train_split_part(split_phase, split_images):
            """
            1) labels/phase/part.txt
            2) labels/phase/part_lmdb
            3) mean/phase/part_mean.{npy, binaryproto}
            """
            label_phase_dir = labels_dir + '/train_test/' + split_phase
            if not os.path.isdir(label_phase_dir):
                os.makedirs(label_phase_dir)

            label_name = label_phase_dir + "/" + part + ".txt"
            with open(label_name, 'w') as l:
                for image in split_images:
                    name, ext = os.path.splitext(image)
                    items = name.split('_')
                    l.write('%s %s\n' % (image, items[2]))

            # post process
            print "==> post processing ", part
            postproc.post_proc(images_dir=part_dir,
                               label_path=label_name,
                               db_path=label_phase_dir + '/' + part + "_lmdb",
                               mean_dir=mean_dir + '/train_test/' + split_phase, mean_name=part + '_mean',
                               width=224,
                               height=224
                               )

        post_proc_train_split_part("train", train_images)
        post_proc_train_split_part("test", test_images)


def generate_train_test_split(pipa_phase):
    """
    Split pipa test set to train/test set (for SVM)
    :return: textfile, list of [pipa file name] + _ + [identity] + .jpg
    """
    # if is_cached:
    #     train_data = hkl.load(labels_dir + "/" + pipa_phase + "/train_file_names.hkl")
    #     test_data = hkl.load(labels_dir + "/" + pipa_phase + "/test_file_names.hkl")
    #     train_labels = hkl.load(labels_dir + "/" + pipa_phase + "/train_labels.hkl")
    #     test_labels = hkl.load(labels_dir + "/" + pipa_phase + "/test_labels.hkl")

    # get split list
    data_list = "split_" + pipa_phase + "_original.txt"
    with open(data_list) as fr:
        lines = fr.readlines()

    lines = [line.rstrip() for line in lines]

    train_data = []
    train_labels = []
    test_data = []
    test_labels = []
    part_list = PartInformation.part_list()
    num_skip = 0
    print "==> separate tran/test set."
    for idx, line in enumerate(lines):

        # this line contains instance information
        items = line.split(' ')
        # print line
        # print items

        # get file name of this instance
        file_name = items[anno_photoset_id] + "_" + items[anno_photo_id] + "_" + items[anno_identity_id] + ".jpg"

        def is_image_exist(filename):
            for part in part_list:
                if not os.path.isfile(images_dir + '/' + pipa_phase + '/' + part + '/' + filename):
                    return False
            return True

        # check if all parts has this instance
        if not is_image_exist(file_name):
            continue

        # separate this instance
        if items[anno_split] == '0':
            train_data.append(file_name)
            train_labels.append(int(items[anno_identity_id]))
        elif items[anno_split] == '1':
            test_data.append(file_name)
            test_labels.append(int(items[anno_identity_id]))
        else:
            raise Exception

        if (idx + 1) % 1000 == 0:
            print "processed", idx + 1

    # save labels
    train_labels = np.asarray(train_labels)
    test_labels = np.asarray(test_labels)
    hkl.dump(train_data, labels_dir + "/" + pipa_phase + "/train_file_names.hkl", mode="w")
    hkl.dump(test_data, labels_dir + "/" + pipa_phase + "/test_file_names.hkl", mode="w")
    hkl.dump(train_labels, labels_dir + "/" + pipa_phase + "/train_labels.hkl", mode="w")
    hkl.dump(test_labels, labels_dir + "/" + pipa_phase + "/test_labels.hkl", mode="w")
    print "train_label size : ", len(train_labels)
    print "test_label size : ", len(test_labels)
    print 'skipped %d images' % num_skip
    return train_data, test_data


def extract_head_features_iter(frontal_net, non_frontal_net, data_list, pipa_phase="test", phase="train"):
    """
    attempt networks separately for faces(frontal/non frontal)

    :param frontal_net:
    :param non_frontal_net:
    :param data_list:
    :param pipa_phase:
    :param phase:
    :return:
    """
    start = time.time()
    feat_list = []
    num_frontal = 0
    num_non_frontal = 0
    if len(data_list) < 1:
        print "data is empty, skipping"
        return None
    for idx, data in enumerate(data_list):
        data = images_dir + "/" + pipa_phase + "/head/" + data
        bgr_img = cv2.imread(data)
        if bgr_img is None:
            raise Exception("Unable to load image: {}".format(data))

        rgb_img = cv2.cvtColor(bgr_img, cv2.COLOR_BGR2RGB)
        aligned_face = align.align(
            imgDim=96,
            rgbImg=rgb_img,
            landmarkIndices=openface.AlignDlib.OUTER_EYES_AND_NOSE)

        if aligned_face is None:
            feat = extract_features(net=non_frontal_net, data=data)
            num_non_frontal += 1
        else:
            feat = extract_features(net=frontal_net, data=data)
            num_frontal += 1

        if (idx + 1) % 100 == 0:
            print "processed %s %s : %d" % ("head", phase, idx + 1)
        feat_list.append(feat)

    end = time.time()
    ms = (end - start) * 1000
    log_time(ms, part_name="head", phase=phase, feat_list=feat_list)
    print "extract head(%s) done " % phase
    print '[frontal : %d ] [non_frontal : %d ] [total : %d ]\n' % \
          (num_frontal, num_non_frontal, num_frontal + num_non_frontal)
    return feat_list


def extract_features_iter(net, part_name, data_list, pipa_phase="test", phase="train"):
    if len(data_list) < 1:
        print "data is zero, skipping"
    start = time.time()
    feat_list = []
    for idx, data in enumerate(data_list):
        feat = extract_features(net=net, data=images_dir + "/" + pipa_phase + "/" + part_name + "/" + data)
        feat_list.append(feat)
        if (idx + 1) % 100 == 0:
            print "processed %s %s : %d" % (part_name, phase, idx + 1)
    end = time.time()
    ms = (end - start) * 1000
    log_time(ms, part_name=part_name, phase=phase, feat_list=feat_list)
    return feat_list


def extract_features(net, data):
    part_img = caffe.io.load_image(data)
    part_feat = net.extract_feature(part_img)
    fmax = max(part_feat)
    return part_feat / fmax


def log_time(ms, part_name, phase, feat_list):
    print "took %f ms in %d images : %f" % (ms, len(feat_list), ms / len(feat_list))
    with open('time.txt', 'a') as f:
        f.write(time.strftime("%Y-%m-%d %H:%M:%S "))
        f.write("%s %s %f\n\n" % (part_name, phase, ms / len(feat_list)))


# noinspection PyTypeChecker
def concatenate_features(features_list):
    # print type(features_list)
    # print type(features_list[0])
    # check all part features has same length(same sample)
    for idx in range(0, len(features_list)):
        if idx == len(features_list) - 1:
            break
        assert len(features_list[idx]) == len(features_list[idx])

    # collect all samples concat feature
    concat_feats_list = []
    for sample_idx in range(0, len(features_list[0])):  # iterate first part features base ex)head
        concat_feats = None
        for idx, part_feat_list in enumerate(features_list):
            if concat_feats is None:
                concat_feats = np.asarray(part_feat_list[sample_idx])
                continue
            concat_feats = np.append(concat_feats,
                                     np.asarray(part_feat_list[sample_idx]), axis=0)  # n_part X 4096 1 dimen
        concat_feats_list.append(concat_feats)

    print "4096 X %d = %d" % (len(features_list), 4096 * len(features_list))
    print "concat : ", concat_feats_list[0].shape
    return concat_feats_list
    # for feats in features_list:
    #     cfeat = np.expand_dims(feats, axis=2)
    #     concat_feats.append(cfeat)
    # return np.concatenate(concat_feats, axis=2)  # n_sample X 4096 X num_of_parts


def extract_all_part_feature(train_data, test_data, pipa_phase="test"):
    """
    :param train_data: List which contains [pipa file name] + _ + [identity] + .jpg
    :param test_data: List which contains [pipa file name] + _ + [identity] + .jpg
    :param pipa_phase:
    :return:
    """
    concat_train = []
    concat_test = []
    print "extract all features."

    prefix = features_dir + "/" + pipa_phase + "/features_"

    # HEAD (VGG + ALEX)
    # print "extracting head."
    # frontal_head = fe.CaffeFeatureExtractor(
    #     model_path=models_dir + "/VGG_FACE_deploy.prototxt",
    #     pretrained_path=weight_dir + "/VGG_FACE.caffemodel",
    #     blob="fc6",
    #     crop_size=224,
    #     meanfile_path=mean_dir + "/" + pipa_phase + "/head_mean.npy"
    # )
    # non_frontal_head = fe.CaffeFeatureExtractor(
    #     model_path=models_dir + "/alexnet_feature.prototxt",
    #     pretrained_path=weight_dir + "/head.caffemodel",
    #     blob="fc7",
    #     crop_size=227,
    #     meanfile_path=mean_dir + "/" + pipa_phase + "/head_mean.npy"
    # )
    # features_train = extract_head_features_iter(frontal_net=frontal_head, non_frontal_net=non_frontal_head,
    #                                             data_list=train_data,
    #                                             pipa_phase=pipa_phase,
    #                                             phase="train")
    # hkl.dump(features_train, prefix + PartInformation.part_head + "_train.hkl", mode="w")
    # features_test = extract_head_features_iter(frontal_net=frontal_head, non_frontal_net=non_frontal_head,
    #                                            data_list=test_data,
    #                                            pipa_phase=pipa_phase,
    #                                            phase="test")
    # hkl.dump(features_test, prefix + PartInformation.part_head + "_test.hkl", mode="w")
    # features_train = hkl.load(prefix + PartInformation.part_head + "_train.hkl")
    # features_test = hkl.load(prefix + PartInformation.part_head + "_test.hkl")
    # assert len(train_data) == len(features_train)
    # assert len(test_data) == len(features_test)
    # concat_train.append(features_train)
    # concat_test.append(features_test)

    # VGG ONLY
    frontal_head = fe.CaffeFeatureExtractor(
        model_path=models_dir + "/VGG_FACE_deploy.prototxt",
        # model_path=models_dir + "/VGG_head_and_upper_deploy.prototxt",
        # pretrained_path=weight_dir + "/VGG_FACE.caffemodel",
        pretrained_path=models_dir + "/snapshot/face_snapshot/face_iter_198000.caffemodel",  # trained on pipa face only
        # pretrained_path=models_dir + "/snapshot/head_and_upper/snapshot_iter_40000.caffemodel",  # trained on face and upper body at the sametime
        # pretrained_path=models_dir + "/snapshot/body_vgg_snapshot/upper_body_iter_30000.caffemodel",  # trained on face, and upper body downgrade on face acc
        # pretrained_path=models_dir + "/snapshot/backup/body_iter_29000.caffemodel",  # trained on body first, bad at face acc
        # blob="fc6",
        # blob="fc7_pipa",
        blob="fc7",
        crop_size=224,
        meanfile_path=mean_dir + "/" + pipa_phase + "/head_mean.npy"
    )
    features_train = extract_features_iter(net=frontal_head, part_name=PartInformation.part_head,
                                           data_list=train_data,
                                           pipa_phase=pipa_phase, phase="train")
    hkl.dump(features_train, prefix + PartInformation.part_head_vgg_only + "_train.hkl", mode="w")
    features_test = extract_features_iter(net=frontal_head, part_name=PartInformation.part_head,
                                          data_list=test_data,
                                          pipa_phase=pipa_phase, phase="test")
    hkl.dump(features_test, prefix + PartInformation.part_head_vgg_only + "_test.hkl", mode="w")
    # features_train = hkl.load(prefix + PartInformation.part_head_vgg_only + "_train.hkl")
    # features_test = hkl.load(prefix + PartInformation.part_head_vgg_only + "_test.hkl")
    assert len(train_data) == len(features_train)
    assert len(test_data) == len(features_test)
    concat_train.append(features_train)
    concat_test.append(features_test)

    # VGG UPPER BODY
    vgg_upper_body = fe.CaffeFeatureExtractor(
        # model_path=models_dir + "/VGG_body_deploy.prototxt",
        # model_path=models_dir + "/VGG_head_and_upper_deploy.prototxt",
        model_path=models_dir + "/VGG_FACE_deploy.prototxt",
        pretrained_path=models_dir + "/snapshot/face_snapshot/face_iter_198000.caffemodel",
        # pretrained_path=models_dir + "/snapshot/head_and_upper/snapshot_iter_40000.caffemodel",
        # pretrained_path=models_dir + "/snapshot/body_vgg_snapshot/upper_body_iter_30000.caffemodel",
        # pretrained_path=models_dir + "/snapshot/body_vgg_snapshot/body_iter_15000.caffemodel",
        # pretrained_path=models_dir + "/snapshot/face_snapshot/face_afterbody_iter_18000.caffemodel",
        # pretrained_path=models_dir + "/snapshot/backup/body_iter_29000.caffemodel",
        # pretrained_path=weight_dir + "/VGG_FACE.caffemodel",
        # blob="fc6",
        blob="fc7",
        # blob="fc7_pipa",
        crop_size=224,
        # meanfile_path=mean_dir + "/" + pipa_phase + "/body_mean.npy"
        meanfile_path=mean_dir + "/" + pipa_phase + "/head_mean.npy"
    )
    features_train = extract_features_iter(net=vgg_upper_body, part_name=PartInformation.part_upper_body,
                                           data_list=train_data,
                                           pipa_phase=pipa_phase, phase="train")
    hkl.dump(features_train, prefix + PartInformation.part_vgg_upper_body + "_train.hkl", mode="w")
    features_test = extract_features_iter(net=vgg_upper_body, part_name=PartInformation.part_upper_body,
                                          data_list=test_data,
                                          pipa_phase=pipa_phase, phase="test")
    hkl.dump(features_test, prefix + PartInformation.part_vgg_upper_body + "_test.hkl", mode="w")
    # features_train = hkl.load(prefix + PartInformation.part_vgg_upper_body + "_train.hkl")
    # features_test = hkl.load(prefix + PartInformation.part_vgg_upper_body + "_test.hkl")
    assert len(train_data) == len(features_train)
    assert len(test_data) == len(features_test)
    concat_train.append(features_train)
    concat_test.append(features_test)

    # VGG BODY
    vgg_body = fe.CaffeFeatureExtractor(
        # model_path=models_dir + "/VGG_body_deploy.prototxt",
        model_path=models_dir + "/VGG_FACE_deploy.prototxt",
        # pretrained_path=models_dir + "/snapshot/body_vgg_snapshot/upper_body_iter_30000.caffemodel",
        pretrained_path=models_dir + "/snapshot/face_snapshot/face_iter_198000.caffemodel",
        # pretrained_path=models_dir + "/snapshot/body_vgg_snapshot/body_iter_15000.caffemodel",
        # pretrained_path=models_dir + "/snapshot/face_snapshot/face_afterbody_iter_18000.caffemodel",
        # pretrained_path=models_dir + "/snapshot/backup/body_iter_29000.caffemodel",
        # pretrained_path=weight_dir + "/VGG_FACE.caffemodel",
        # blob="fc6",
        blob="fc7",
        # blob="fc7_pipa",
        crop_size=224,
        # meanfile_path=mean_dir + "/" + pipa_phase + "/body_mean.npy"
        meanfile_path=mean_dir + "/" + pipa_phase + "/head_mean.npy"
    )
    features_train = extract_features_iter(net=vgg_body, part_name=PartInformation.part_body,
                                           data_list=train_data,
                                           pipa_phase=pipa_phase, phase="train")
    hkl.dump(features_train, prefix + PartInformation.part_vgg_body + "_train.hkl", mode="w")
    features_test = extract_features_iter(net=vgg_body, part_name=PartInformation.part_body,
                                          data_list=test_data,
                                          pipa_phase=pipa_phase, phase="test")
    hkl.dump(features_test, prefix + PartInformation.part_vgg_body + "_test.hkl", mode="w")
    # features_train = hkl.load(prefix + PartInformation.part_vgg_body + "_train.hkl")
    # features_test = hkl.load(prefix + PartInformation.part_vgg_body + "_test.hkl")
    assert len(train_data) == len(features_train)
    assert len(test_data) == len(features_test)
    concat_train.append(features_train)
    concat_test.append(features_test)

    # ALEX ONLY
    # frontal_head = fe.CaffeFeatureExtractor(
    #     model_path=models_dir + "/alexnet_feature.prototxt",
    #     pretrained_path=weight_dir + "/head.caffemodel",
    #     blob="fc6",
    #     crop_size=224,
    #     meanfile_path=mean_dir + "/" + pipa_phase + "/head_mean.npy"
    # )
    # features_train = extract_features_iter(net=frontal_head, part_name=PartInformation.part_head,
    #                                        data_list=train_data,
    #                                        pipa_phase=pipa_phase, phase="train")
    # hkl.dump(features_train, prefix + PartInformation.part_vgg_only + "_train.hkl", mode="w")
    # features_test = extract_features_iter(net=frontal_head, part_name=PartInformation.part_head, data_list=test_data,
    #                                       pipa_phase=pipa_phase, phase="test")
    # hkl.dump(features_test, prefix + PartInformation.part_vgg_only + "_test.hkl", mode="w")
    # features_train = hkl.load(prefix + PartInformation.part_vgg_only + "_train.hkl")
    # features_test = hkl.load(prefix + PartInformation.part_vgg_only + "_test.hkl")
    # assert len(train_data) == len(features_train)
    # assert len(test_data) == len(features_test)
    # concat_train.append(features_train)
    # concat_test.append(features_test)

    # Head attr (casia)
    # features_train, features_test = extract_features_alexnet(is_cached=True, prefix=prefix, pipa_phase=pipa_phase,
    #                                                          part=PartInformation.part_head_casia,
    #                                                          train_data=train_data,
    #                                                          test_data=test_data,
    #                                                          part_parent_name=PartInformation.part_head)
    # concat_train.append(features_train)
    # concat_test.append(features_test)

    # Head attr (cacd)
    # features_train, features_test = extract_features_alexnet(is_cached=True, prefix=prefix, pipa_phase=pipa_phase,
    #                                                          part=PartInformation.part_head_cacd,
    #                                                          train_data=train_data,
    #                                                          test_data=test_data,
    #                                                          part_parent_name=PartInformation.part_head)
    # concat_train.append(features_train)
    # concat_test.append(features_test)

    # Head attr (age)
    # features_train, features_test = extract_features_alexnet(is_cached=True, prefix=prefix, pipa_phase=pipa_phase,
    #                          part=PartInformation.part_head_age, train_data=train_data, test_data=test_data,
    #                          part_parent_name=PartInformation.part_head)
    # concat_train.append(features_train)
    # concat_test.append(features_test)

    # Head attr (gender)
    # features_train, features_test = extract_features_alexnet(is_cached=True, prefix=prefix, pipa_phase=pipa_phase,
    #                          part=PartInformation.part_head_gender, train_data=train_data, test_data=test_data,
    #                          part_parent_name=PartInformation.part_head)
    # concat_train.append(features_train)
    # concat_test.append(features_test)

    # Head attr (glasses)
    # features_train, features_test = extract_features_alexnet(is_cached=True, prefix=prefix, pipa_phase=pipa_phase,
    #                          part=PartInformation.part_head_glasses, train_data=train_data, test_data=test_data,
    #                          part_parent_name=PartInformation.part_head)
    # concat_train.append(features_train)
    # concat_test.append(features_test)

    # Head attr (hair color)
    # features_train, features_test = extract_features_alexnet(is_cached=True, prefix=prefix, pipa_phase=pipa_phase,
    #                          part=PartInformation.part_head_haircolour, train_data=train_data, test_data=test_data,
    #                          part_parent_name=PartInformation.part_head)
    # concat_train.append(features_train)
    # concat_test.append(features_test)

    # Head attr (hair length)
    # features_train, features_test = extract_features_alexnet(is_cached=True, prefix=prefix, pipa_phase=pipa_phase,
    #                          part=PartInformation.part_head_hairlength, train_data=train_data, test_data=test_data,
    #                          part_parent_name=PartInformation.part_head)
    # concat_train.append(features_train)
    # concat_test.append(features_test)

    # UPPER BODY
    # print "extracting upper body."
    # features_train, features_test = extract_features_alexnet(is_cached=True, prefix=prefix, pipa_phase=pipa_phase,
    #                          part=PartInformation.part_upper_body, train_data=train_data, test_data=test_data)
    # concat_train.append(features_train)
    # concat_test.append(features_test)

    # Upper attr (age1)
    # features_train, features_test = extract_features_alexnet(is_cached=True, prefix=prefix, pipa_phase=pipa_phase,
    #                          part=PartInformation.part_upper_body_age1, train_data=train_data, test_data=test_data,
    #                          part_parent_name=PartInformation.part_upper_body)
    # concat_train.append(features_train)
    # concat_test.append(features_test)

    # Upper attr (age2)
    # features_train, features_test = extract_features_alexnet(is_cached=True, prefix=prefix, pipa_phase=pipa_phase,
    #                          part=PartInformation.part_upper_body_age2, train_data=train_data, test_data=test_data,
    #                          part_parent_name=PartInformation.part_upper_body)
    # concat_train.append(features_train)
    # concat_test.append(features_test)

    # Upper attr (gender)
    # features_train, features_test = extract_features_alexnet(is_cached=True, prefix=prefix, pipa_phase=pipa_phase,
    #                          part=PartInformation.part_upper_body_gender, train_data=train_data, test_data=test_data,
    #                          part_parent_name=PartInformation.part_upper_body)
    # concat_train.append(features_train)
    # concat_test.append(features_test)

    # Upper attr (hairshort)
    # features_train, features_test = extract_features_alexnet(is_cached=True, prefix=prefix, pipa_phase=pipa_phase,
    #                          part=PartInformation.part_upper_body_hairshort,
    #                          train_data=train_data, test_data=test_data,
    #                          part_parent_name=PartInformation.part_upper_body)
    # concat_train.append(features_train)
    # concat_test.append(features_test)

    # Upper attr (hairblack)
    # features_train, features_test = extract_features_alexnet(is_cached=True, prefix=prefix, pipa_phase=pipa_phase,
    #                          part=PartInformation.part_upper_body_hairblack,
    #                          train_data=train_data, test_data=test_data,
    #                          part_parent_name=PartInformation.part_upper_body)
    # concat_train.append(features_train)
    # concat_test.append(features_test)

    # BODY
    # print "extracting body."
    # features_train, features_test = extract_features_alexnet(is_cached=True, prefix=prefix, pipa_phase=pipa_phase,
    #                          part=PartInformation.part_body, train_data=train_data, test_data=test_data)
    # concat_train.append(features_train)
    # concat_test.append(features_test)

    # FACE
    # print "extracting face"

    # SCENE
    # print "extracting scene"
    # features_train, features_test = extract_features_alexnet(is_cached=True, prefix=prefix, pipa_phase=pipa_phase,
    #                          part=PartInformation.part_scene, train_data=train_data, test_data=test_data)
    # concat_train.append(features_train)
    # concat_test.append(features_test)

    # check validity about these features
    print "=====> train features result "
    for idx, feats in enumerate(concat_train):
        print "%d'th feature size : %d" % (idx, len(feats[0]))
    print "=====> test features result "
    for idx, feats in enumerate(concat_test):
        print "%d'th feature size : %d" % (idx, len(feats[0]))

    # concatenating
    print "concatenating all features"
    concat_train = concatenate_features(concat_train)
    hkl.dump(concat_train, prefix + "concat_train.hkl", mode="w")
    print "concat train : ", len(concat_train)
    concat_test = concatenate_features(concat_test)
    hkl.dump(concat_test, prefix + "concat_test.hkl", mode="w")
    print "concat test : ", len(concat_test)


def extract_features_alexnet(is_cached, prefix, pipa_phase, part, train_data,
                             test_data, part_parent_name=None, net=None):
    """

    :param net:
    :param is_cached:
    :param prefix:
    :param pipa_phase:
    :param part: with suffix, used for result hkl name, or load caffemodel (ex : head_age)
    :param train_data:
    :param test_data:
    :param part_parent_name: used for iterating img (ex: head)
    :return:
    """
    if not is_cached:
        if part_parent_name is None:
            part_parent_name = part
        if net is None:
            net = fe.CaffeFeatureExtractor(
                model_path=models_dir + "/alexnet_feature.prototxt",
                pretrained_path=weight_dir + "/" + part + ".caffemodel",
                blob="fc7",
                crop_size=227,
                meanfile_path=mean_dir + "/" + pipa_phase + "/" + part_parent_name + "_mean.npy"
            )
        features_train = extract_features_iter(net, part_parent_name, train_data,
                                               pipa_phase=pipa_phase, phase="train")
        hkl.dump(features_train, prefix + part + "_train.hkl", mode="w")
        features_test = extract_features_iter(net, part_parent_name, test_data,
                                              pipa_phase=pipa_phase, phase="test")
        hkl.dump(features_test, prefix + part + "_test.hkl", mode="w")
    else:
        features_train = hkl.load(prefix + part + "_train.hkl")
        features_test = hkl.load(prefix + part + "_test.hkl")
    assert len(train_data) == len(features_train)
    assert len(test_data) == len(features_test)
    return features_train, features_test


def mk_dir(*args):
    # for arg in args:
    #     if not os.path.isdir(arg):
    #         os.mkdir(arg)
    pass


# noinspection PyUnusedLocal
def execute(phase):

    # you may set this before launch
    # phase = "test"
    # phase = "val"

    print "excuting pipa ", phase
    images = raw_input('cleaning images and get new images : (y/n) ')
    post = raw_input('doing post process : (y/n) ')
    feat = raw_input('extracting features : (y/n) ')

    # if images are already prepared skip this
    # here we make all parts images.
    if images == 'Y' or images == 'y':
        # get_images(part_list=PartInformation.part_list(), phase=phase)
        print "extracting images done."

    # post proc
    if post == 'Y' or post == 'y':
        # post_proc(PartInformation.part_list(), phase=phase)
        # post_proc([PartInformation.part_scene], phase=phase)
        # post_proc_train_split(PartInformation.part_list())
        # post_proc_combine_part([PartInformation.part_head, PartInformation.part_upper_body])
        print "post processing images done."

    # extract features.
    if feat == 'Y' or feat == 'y':
        train_data, test_data = generate_train_test_split(phase)
        extract_all_part_feature(train_data=train_data, test_data=test_data, pipa_phase=phase)
        print 'extracting features done.'

    print "all done."


if __name__ == "__main__":

    mk_dir(result_dir, images_dir, models_dir, weight_dir, mean_dir, features_dir, labels_dir)

    # openFace align models
    modelDir = os.path.join('/home/shlee/Documents/openface-master', 'models')
    dlibModelDir = os.path.join(modelDir, 'dlib')
    # openfaceModelDir = os.path.join(modelDir, 'openface')
    align = openface.AlignDlib(os.path.join(
        dlibModelDir,
        "shape_predictor_68_face_landmarks.dat"))

    # annotations
    anno_photoset_id = 0
    anno_photo_id = 1
    anno_xmin = 2
    anno_ymin = 3
    anno_width = 4
    anno_height = 5
    anno_identity_id = 6
    anno_subset_id = 7
    anno_split = 8

    # execute("train")
    execute("test")
    # execute("val")

